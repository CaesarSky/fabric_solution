from django.contrib import admin
from django.urls import path, include

from config.yasg import urlpatterns as yasg_url

v1_api = (
    [
        path("accounts/", include("apps.accounts.urls")),
        path("mailing/", include("apps.mailing.urls")),
    ],
    "v1",
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/", include(v1_api, namespace="v1")),
]

urlpatterns += yasg_url
