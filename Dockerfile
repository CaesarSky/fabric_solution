FROM python:3.11-slim

ENV PYTHONUNBUFFERED 1
WORKDIR /fabric

RUN apt-get update -qq && apt-get install --no-install-recommends apt-utils curl gnupg2 gcc python3-dev libpq-dev python3-wheel \
    python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev \
    binutils libproj-dev gdal-bin \
    shared-mime-info musl-dev libsdl-pango-dev -y

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
ENV PATH "$PATH:/root/.local/bin/"

COPY pyproject.toml .
COPY poetry.lock .
RUN poetry config virtualenvs.create false \
  && poetry config installer.parallel false \
  && poetry install --no-interaction --no-ansi


COPY . /fabric
