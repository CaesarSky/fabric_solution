## Name
Фабрика Решений

## Installation
- > git clone https://gitlab.com/CaesarSky/fabric_solution.git
- > cd fabric_solution

## Usage
The use case is easy if you have installed poetry in your local system.     
`poetry install`    
`poetry shell`  
`python manage.py runserver`    
If you don't have poetry installed in your system, you can use it with docker, docker-compose. For example:     
`docker-compose up --build -d`

Swagger stored in [localhost:8000/swagger/](localhost:8000/swagger/)
