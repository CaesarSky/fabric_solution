from django.core.validators import RegexValidator

mobile_regex = RegexValidator(
    regex=r"^\+?1?\d{10,11}$",
    message="Phone number must not consist of space and requires country code. eg : +71234567890",
)
