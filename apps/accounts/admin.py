from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import CustomUser, Tag, MobileOperator


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("tag",)


@admin.register(MobileOperator)
class MobileOperatorAdmin(admin.ModelAdmin):
    list_display = ("code",)


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    """
    Creating own admin and customise it to our own works
    """

    readonly_fields = ["date_joined"]
    fieldsets = (
        (
            None,
            {"fields": ("phone_number", "password")},
        ),
        (
            _("Персональная информация"),
            {"fields": ("full_name", "mobile_operator", "tag", "time_zone")},
        ),
        (
            _("Разрешения"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (_("Главные даты"), {"fields": ("date_joined",)}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("phone_number", "password1", "password2"),
            },
        ),
    )
    list_display = ("phone_number", "is_staff", "date_joined")
    search_fields = ("phone_number",)
    ordering = ("phone_number",)
