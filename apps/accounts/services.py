from django.contrib.auth import get_user_model
from rest_framework.generics import get_object_or_404

from apps.accounts.models import MobileOperator, Tag

User = get_user_model()


class UserCreateService:
    def __init__(
        self,
        phone_number: str,
        full_name: str,
        time_zone: str,
        mobile_operator: int,
        tag: str,
    ):
        self.phone_number = phone_number
        self.full_name = full_name
        self.time_zone = time_zone
        self.mobile_operator = mobile_operator
        self.tag = tag

    def __get_mobile_operator(self) -> MobileOperator:
        return get_object_or_404(MobileOperator, code=self.mobile_operator)

    def __get_tag(self) -> Tag:
        return get_object_or_404(Tag, tag=self.tag)

    def create_user(self) -> User:
        mobile_operator = self.__get_mobile_operator()
        tag = self.__get_tag()
        user = User.objects.create(
            full_name=self.full_name,
            phone_number=self.phone_number,
            time_zone=self.time_zone,
            mobile_operator=mobile_operator,
            tag=tag,
        )
        return user


class UserUpdateService:
    def __init__(self, user: User, data: dict):
        self.user = user
        self.data = data

    def __get_tag(self, tag: str):
        return get_object_or_404(Tag, tag=tag)

    def __get_mobile_operator(self, mobile_operator: int):
        return get_object_or_404(MobileOperator, code=mobile_operator)

    def update_user(self):
        mobile_operator = self.data.pop("mobile_operator", None)
        tag = self.data.pop("tag", None)
        print(mobile_operator, tag)

        if mobile_operator:
            mobile_operator = self.__get_mobile_operator(mobile_operator)
            self.user.mobile_operator = mobile_operator
        if tag:
            tag = self.__get_tag(tag)
            self.user.tag = tag

        for key, value in self.data.items():
            setattr(self.user, key, value)
        self.user.save()
        return self.user


class UserDeleteService:
    def __init__(self, user: User):
        self.user = user

    def delete_user(self):
        self.user.delete()
        return "Deleted"
