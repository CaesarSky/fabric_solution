from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from timezone_field.rest_framework import TimeZoneSerializerField

from apps.accounts.models import MobileOperator, Tag, CustomUser
from apps.accounts.validators import mobile_regex


class MobileOperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = MobileOperator
        fields = ("code",)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ("tag",)


class UserSerializer(serializers.Serializer):
    mobile_operator = serializers.IntegerField(required=False)
    tag = serializers.CharField(max_length=200, required=False)
    time_zone = TimeZoneSerializerField(use_pytz=True, required=False)
    full_name = serializers.CharField(required=False, max_length=255)
    phone_number = serializers.CharField(
        required=True, max_length=20, validators=(mobile_regex,)
    )
