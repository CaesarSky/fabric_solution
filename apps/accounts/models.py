from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

from timezone_field import TimeZoneField

from apps.accounts.managers import CustomUserManager
from apps.accounts.validators import mobile_regex


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model based on our new feature
    Rewrite django abstract base user for our own needs
    """

    full_name = models.CharField(
        verbose_name="ФИО", max_length=255, null=True, blank=True
    )
    phone_number = models.CharField(
        verbose_name="Номер телефона",
        max_length=20,
        unique=True,
        validators=(mobile_regex,),
    )
    time_zone = TimeZoneField(
        verbose_name="Часовой пояс",
        choices_display="WITH_GMT_OFFSET",
        default="Europe/Moscow",
    )
    mobile_operator = models.ForeignKey(
        "MobileOperator",
        verbose_name="Мобильный оператор",
        on_delete=models.SET_NULL,
        related_name="users",
        null=True,
    )
    tag = models.ForeignKey(
        "Tag",
        verbose_name="Тэг",
        on_delete=models.SET_NULL,
        related_name="users",
        null=True,
    )
    is_staff = models.BooleanField(
        "Является пользователем",
        default=False,
        help_text=(
            "Определяет, может ли пользователь войти на этот сайт администратора."
        ),
    )
    is_active = models.BooleanField(
        "Активный",
        default=True,
        help_text=(
            "Указывает, следует ли считать этого пользователя активным. "
            "Отменить выбор вместо удаления учетных записей."
        ),
    )
    is_superuser = models.BooleanField(verbose_name="Админ", default=False)
    date_joined = models.DateTimeField(
        verbose_name="Дата регистрации", auto_now_add=True
    )

    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self):
        return self.phone_number


class MobileOperator(models.Model):
    """
    Mobile operator model
    that allows us to create and store multiple codes for multiple user's
    """

    code = models.PositiveSmallIntegerField(
        verbose_name="Код мобильного оператора",
        default=7,
        unique=True,
    )

    class Meta:
        verbose_name = "Код мобильного оператора"
        verbose_name_plural = "Коды мобильного оператора"

    def __str__(self):
        return f"{self.code}"


class Tag(models.Model):
    """Didn't properly understand what "tag" exactly means and why we need it"""

    tag = models.CharField(verbose_name="Тэг", max_length=200, unique=True)

    class Meta:
        verbose_name = "Тэг"
        verbose_name_plural = "Тэги"

    def __str__(self):
        return self.tag
