from django.urls import path

from apps.accounts.views import (
    UserAPIVIew,
)

urlpatterns = [
    path("user/", UserAPIVIew.as_view(), name="user-update-create"),
]
