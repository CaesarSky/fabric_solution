from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.settings import api_settings

from apps.accounts.serializers import (
    UserSerializer,
    TagSerializer,
    MobileOperatorSerializer,
)
from apps.accounts.models import CustomUser, Tag, MobileOperator
from apps.accounts.services import (
    UserCreateService,
    UserUpdateService,
    UserDeleteService,
)


class UserAPIVIew(generics.GenericAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        return get_object_or_404(
            CustomUser, phone_number=self.request.data.get("phone_number")
        )

    def get_success_headers(self, data):
        try:
            return {"Location": str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_service = UserCreateService(**serializer.validated_data)
        user_service.create_user()
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data)
        print("hello world")
        serializer.is_valid(raise_exception=True)
        print("hello world")
        user_service = UserUpdateService(instance, serializer.validated_data)
        user_service.update_user()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_service = UserDeleteService(instance)
        user_service.delete_user()
        return Response("Удален", status=status.HTTP_204_NO_CONTENT)
