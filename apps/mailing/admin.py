from django.contrib import admin

from apps.mailing.models import MailingList, Message


@admin.register(MailingList)
class MailingListAdmin(admin.ModelAdmin):
    list_display = ("message", "mailing_filter")


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ("__str__",)
