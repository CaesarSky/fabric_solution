from rest_framework import serializers

from apps.accounts.serializers import UserSerializer
from apps.mailing.models import MailingList, Message


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    mailing_list = MailingListSerializer()
    user = UserSerializer()

    class Meta:
        model = Message
        fields = (
            "mailing_list",
            "user",
            "status",
        )
