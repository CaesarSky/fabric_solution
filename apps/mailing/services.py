from django.db.models import Count, Q

from apps.mailing.models import MailingList


class MailingListSendService:
    """
    It depends on where we should send message.
    If it's email or another service we can implement background tasks such as Huey or Celery to handle errors
    and send something via some time
    """

    pass


class MailingListStatisticService:
    def get_stats(self):
        statistics = MailingList.objects.annotate(
            total_messages=Count("messages"),
            sent_messages=Count("messages", filter=Q(messages__status=True)),
            unsent_messages=Count(
                "messages", filter=Q(messages__status=False)
            ),
        ).values(
            "id",
            "start_date",
            "end_date",
            "total_messages",
            "sent_messages",
            "unsent_messages",
        )
        print(statistics)
        return statistics
