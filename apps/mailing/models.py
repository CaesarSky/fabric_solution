from django.contrib.auth import get_user_model
from django.db import models

from apps.accounts.choices import MAILING_FILTER_CHOICES


User = get_user_model()


class MailingList(models.Model):
    start_date = models.DateTimeField(
        verbose_name="Дата и время запуска рассылки"
    )
    end_date = models.DateTimeField(
        verbose_name="Дата и время окончания рассылки"
    )
    message = models.TextField(verbose_name="Сообщение")
    mailing_filter = models.PositiveSmallIntegerField(
        verbose_name="Фильтрация свойств клиентов (Код мобильного оператора, тег)",
        choices=MAILING_FILTER_CHOICES,
    )

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return self.message


class Message(models.Model):
    mailing_list = models.ForeignKey(
        MailingList,
        verbose_name="Сообщения",
        on_delete=models.CASCADE,
        related_name="messages",
    )
    user = models.ForeignKey(
        User,
        verbose_name="Пользователь",
        on_delete=models.CASCADE,
        related_name="messages",
    )
    status = models.BooleanField(verbose_name="Отправлен", default=False)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return (
            f"Сообщение для {self.user.phone_number}. "
            f"сообщение: {self.mailing_list.message}"
        )
