from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status

from apps.mailing.serializers import MailingListSerializer
from apps.mailing.models import MailingList
from apps.mailing.services import (
    MailingListStatisticService,
    MailingListSendService,
)


class MailingListCreateAPIView(generics.CreateAPIView):
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer


class MailingListStatsAPIView(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        statistics_service = MailingListStatisticService()
        return Response(
            statistics_service.get_stats(), status=status.HTTP_200_OK
        )


class MailingListSendAPIView(generics.GenericAPIView):
    """Not properly understand where i should send messages"""

    def post(self, request, *args, **kwargs):
        send_service = MailingListSendService()
        return Response("New sent", status=status.HTTP_200_OK)
