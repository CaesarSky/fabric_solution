from django.urls import path

from apps.mailing.views import (
    MailingListCreateAPIView,
    MailingListStatsAPIView,
    MailingListSendAPIView,
)

urlpatterns = [
    path(
        "mailing-list/",
        MailingListCreateAPIView.as_view(),
        name="mailing-list-create",
    ),
    path(
        "general-statistic/",
        MailingListStatsAPIView.as_view(),
        name="general_statistic",
    ),
    path(
        "mailing-send/", MailingListSendAPIView.as_view(), name="mailing-send"
    ),
]
